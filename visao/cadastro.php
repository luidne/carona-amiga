<?php
	require_once('Componentes.php');
	$objComponentes = new Componentes();	
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Carona Amiga - Meu Perfil</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="../resources/js/jquery-ui-1.9.1.custom/css/smoothness/jquery-ui-1.9.1.custom.min.css" type="text/css" />
        <link rel="stylesheet" href="../resources/js/jquery-ui-1.9.1.custom/development-bundle/demos/demos.css" type="text/css" />
        <link rel="stylesheet" href="../resources/js/jQuery-Validation-Engine-master/css/validationEngine.jquery.css" type="text/css" />
        <link rel="stylesheet" href="../resources/js/Apprise-1.5.min/apprise.min.css" type="text/css" />
        <link rel="stylesheet" href="../resources/metro/css/modern.css">
        <link rel="stylesheet" href="../resources/metro/css/modern-responsive.css">
        <link rel="stylesheet" href="../resources/meu_estilo.css">
        

		<script type="text/javascript" src="../resources/js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="../resources/js/jquery-ui-1.9.1.custom/js/jquery-ui-1.9.1.custom.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery-Validation-Engine-master/js/languages/jquery.validationEngine-pt_BR.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery-Validation-Engine-master/js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="../resources/js/Apprise-1.5.min/apprise-1.5.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery-Mask-Plugin-master/jquery.mask.min.js"></script>
        <!--<script type="text/javascript" src="../resources/metro/js/carousel.js" ></script>-->
        
        <script type="text/javascript">

		$(document).ready(function() {
			
			$('#txt-cpf').mask('999.999.999-99', {reverse: true});
			$('#txt-data-nascimento').mask('99/99/9999');
			$('#txt-telefone').mask('(99) 9999-9999');
			
			$("#form-cadastro").validationEngine();
			
			
			$('#form-cadastro').submit(function(){		
			
				$('.field').each(function(index, element) {
		
					if($.trim($(this).val()) == "" && isValidou) {				
						
						return false;
					}
				});
				
				if($.trim($('#txt-senha').val()) != $.trim($('#txt-senha2').val())){
					
					apprise('Senhas não coferem!');
					return false;
				}
				
				return true;
				
				/*					
				$.ajax({
					
					type: "POST",
					url: "soma.php",
					async: false,
					data: $(this).serialize(),					
					success: function(e){
						
						alert(e);
										
					},
					error: function(e){
						alert(2)
					}
				});
				*/
				
			});
				
		});
		
		</script>

    </head>
    <body>
        <div class="page secondary back_topo">            
            <div class="page-region">
                <div class=" page page-region-content" style="width: 1250px;">
                    <div class="grid" style="margin-bottom: 0;">
                        <!--[Início] Painel do Cabeçalho -->
                        <div class="row">
                            <div class="span6">
                                <h1>Bem-vindo(a) ao</h1>
                            </div>
                            <div class="span3" style="text-align: center;">
                                <a href="principal.html">
                                    <img src="../resources/img/logo_carona_amiga.png" />
                                </a>
                            </div>
                            <div class="span3" style="text-align: right;">
                                <a href="#">Ajuda</a>
                            </div> 

                        </div>
                        <!--[Fim] Painel do Cabeçalho -->
                    </div>
                    
                    <form id='form-cadastro' method='post' action="cadastroCode.php">    
                    <h2>Cadastro de Conta</h2>
                    <div class="grid ">
                        <div class="row">

                            <div class="span11">
                                <!--[Fim] Alterar Dados da Conta -->
                                <div class="span5">


                                    <div class="span11"> 
                                        <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Dados da Conta</h3>
                                    </div>
                                    <div class="span5">
                                        <table style="width: auto;" >
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Nome:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-nome' id='txt-nome' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Matrícula:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-matricula' id='txt-matricula' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>CPF:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-cpf' id='txt-cpf' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Sexo</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <select name="select-sexo" id="select-sexo">
                                                        	<option value="">Selecione uma opção...</option>
                                                          	<option value="M">Masculino</option>
                                                          	<option value="F">Feminino</option>
                                                        </select> 
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>E-mail:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-email' id='txt-email' />
                                                    </div> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Senha:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control password">
                                                        <input type='password' class="validate[required] field" name='txt-senha' id='txt-senha' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Confirmar Senha:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control password">
                                                        <input type='password' class="validate[required] field" name='txt-senha2' id='txt-senha2' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Telefone:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-telefone' id='txt-telefone' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Data de Nascimento:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-data-nascimento' id='txt-data-nascimento' />
                                                    </div> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Curso:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <?php $objComponentes->getSelectCurso(); ?>
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Período do Curso:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <select name="select-periodo" id="select-periodo" class="validate[required] field" >
                                                        	<option value="">Selecione uma opção...</option>
                                                         	<option value="1">1° Período</option>
                                                          	<option value="2">2° Período</option>
                                                          	<option value="3">3° Período</option>
                                                          	<option value="4">4° Período</option>
                                                          	<option value="5">5° Período</option>
                                                          	<option value="6">6° Período</option>
                                                          	<option value="7">7° Período</option>
                                                          	<option value="8">8° Período</option>
                                                          	<option value="9">9° Período</option>
                                                          	<option value="10">10° Período</option>
                                                        </select>
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Endereço:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' class="validate[required] field" name='txt-endereco' id='txt-endereco' />
                                                    </div>                                          
                                                </td>
                                            </tr>                                            
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Ponto de Saída:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control textarea">
                                                        <textarea name='txtArea-ponto-saida' id='txtArea-ponto-saida' ></textarea>
                                                    </div> 
                                                </td>
                                            </tr>                                                                                                                   
                                        </table>

                                        <hr />

                                    </div>
                                </div>
                                <!--[Fim] Alterar Dados da Conta -->

                                <!--[Início] Alterar Meu Perfil -->
                                <div class="span5">

                                    <div class="span5"> 
                                        <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Meu Perfil</h3>
                                    </div>

                                    <div class="span5">
                                        <table style="width: auto;" >
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Profissão:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' name='txt-profissao' id='txt-profissao' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Local de Trabalho:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text'  name='txt-local-trabalho' id='txt-local-trabalho' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Veículo:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <select name="select-veiculo" id="select-veiculo">
                                                        	<option value="">Selecione uma opção...</option>
                                                          	<option value="carro">Carro</option>
                                                          	<option value="moto">Moto</option>
                                                            <option value="onibus">Ônibus</option>
                                                            <option value="nenhum">Nenhum</option>
                                                        </select>
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Orientação Sexual:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <select name="select-orientacao-sexual" id="select-orientacao-sexual">
                                                        	<option value="">Selecione uma opção...</option>
                                                          	<option value="Heterosexual">Heterosexual</option>
                                                          	<option value="Homosexual">Homosexual</option>
                                                            <option value="Bisexual">Bisexual</option>
                                                        </select>
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Religião:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text'  name='txt-religiao' id='txt-religiao' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Fumante:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                         <select name="select-fumante" id="select-fumante">
                                                        	<option value="">Selecione uma opção...</option>
                                                          	<option value="S">Sim</option>
                                                          	<option value="N">Não</option>
                                                        </select>
                                                    </div> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Esportes:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text'  name='txt-esportes' id='txt-esportes' />
                                                    </div>                                          
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Músicas:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <input type='text' name='txt-musicas' id='txt-musicas' />
                                                    </div> 
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td class="border-color-white" style="text-align: right;">
                                                    <h5>Status:</h5>                                           
                                                </td>
                                                <td class="border-color-white" >
                                                    <div class="input-control text">
                                                        <?php $objComponentes->getSelectStatus(); ?>
                                                    </div> 
                                                </td>
                                            </tr>                                       
                                        </table>

                                        <hr />

                                    </div>
                                </div>
                                <!--[Fim] Alterar Meu Perfil -->
                                <div class="span5">
									<!--<input class="bg-color-green fg-color-white command-button" type="submit" value="oi"/> -->
                                    <button class="bg-color-green fg-color-white command-button">
                                        Cadastrar
                                        <small>e começar a usar</small>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!--[Início] Painel do Rodapé -->
                        <div class="row bg-color-gray" style="text-align: right; width: 100%;">
                            <br/>
                            <br/>
                            <div class="span3">

                            </div>
                            <div class="span11" style="border-top: 1px #ccc dotted;">
                                <div class="horizontal-menu" style="float: center;">
                                    <ul>
                                        <li>
                                            <a class="fg-color-blue" style="" href="http://www.catolica-to.edu.br">
                                                Faculdade Católica do Tocantins
                                            </a>
                                        </li>
                                        <li> 
                                            <a style="color: #cc; font-size: 10pt;">                                  
                                                Curso de Sistemas de Informação
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--[Fim] Painel do Rodapé -->
                    </div> 				
                   	</form>
                </div>
            </div>
        </div>
    </body>
</html>
