<?php
	print_r($_POST);
	require_once('../beans/Cusuario.php');
	require_once('../beans/Cendereco.php');
	require_once('../daos/CenderecoDAO.php');
	require_once('../beans/CpontoDeSaida.php');
	require_once('../daos/CpontoDeSaidaDAO.php');
	require_once('../beans/Cusuario.php');
	require_once('../daos/CusuarioDAO.php');
	require_once('../beans/Ctelefones.php');
	require_once('../daos/CtelefonesDAO.php');
	require_once('../beans/CperfilUsuario.php');
	require_once('../daos/CperfilUsuarioDAO.php');

	
	
	// ENDEREÇO.
	$objEnd = new Cendereco();
	$objEnd->setEndereco(trim($_POST['txt-endereco']));
	
	$objEndDao = new CenderecoDAO();
	$idEndereco = $objEndDao->insert($objEnd);
	
	
	// PONTO DE SAIDA.
	$objPontoSaida = new CpontoDeSaida();
	$objPontoSaida->setDescricao(trim($_POST['txtArea-ponto-saida']));
	$objPontoSaida->setEndereco_id($idEndereco);
	
	$objPontoSaidaDAO = new CpontoDeSaidaDAO();
	$idPontoSaida = $objPontoSaidaDAO->insert($objPontoSaida);
	
	
	// USUARIO.
	$cpf = str_replace('.', '', trim($_POST['txt-cpf']));
	$cpf = str_replace('-', '', $cpf);
	$arrayDataNasc = explode('/', trim($_POST['txt-data-nascimento']));
	$dataNascimento = $arrayDataNasc[2] .'-'. $arrayDataNasc[1] .'-'. $arrayDataNasc[0];
	$objUsuario = new Cusuario();
	$objUsuario->setCpf($cpf);
	$objUsuario->setCursos_id(trim($_POST['select-curso']));
	$objUsuario->setDataNascimento($dataNascimento);
	$objUsuario->setEmail(trim($_POST['txt-email']));	
	$objUsuario->setMatricula(trim($_POST['txt-matricula']));
	$objUsuario->setNome(trim($_POST['txt-nome']));
	$objUsuario->setPeriodoCurso(trim($_POST['select-periodo']));
	$objUsuario->setSenha(trim($_POST['txt-senha']));
	$objUsuario->setSexo(trim($_POST['select-sexo']));
	$objUsuario->setPontoDeSaida_id($idPontoSaida);
	$objUsuario->setEndereco_id($idEndereco);
	
	$objUsuarioDAO = new CusuarioDAO();
	$idUsuario = $objUsuarioDAO->insert($objUsuario);
	
	if($idUsuario > 0) {
		
		// TELEFONE
		$codigoArea = substr(trim($_POST['txt-telefone']), 1, 2);
		$telefone = substr(trim($_POST['txt-telefone']), 4, strlen(trim($_POST['txt-telefone']))-1);
		$telefone = str_replace('-', '', $telefone);
		$objTelefone = new Ctelefones();
		$objTelefone->setCodigoDeArea($codigoArea);
		$objTelefone->setTelefone(trim($telefone));
		$objTelefone->setUsuario_idUsuario($idUsuario);
		
		$objTelefoneDAO = new CtelefonesDAO();
		$cadastrou = 0;
		if($objTelefoneDAO->insert($objTelefone) > 0) {
			$cadastrou++;
		}
		else {
			echo "Falha ao tentar inserir o Telefone";
		}
		
		// PERFIL DO USUARIO
		$objPerfil = new CperfilUsuario();
		$objPerfil->setEsportes(trim($_POST['txt-esportes']));
		$objPerfil->setFumante(trim($_POST['select-fumante']));
		$objPerfil->setLocalDeTrabalho(trim($_POST['txt-local-trabalho']));
		$objPerfil->setMusicas(trim($_POST['txt-musicas']));
		$objPerfil->setOrientacaoSexual(trim($_POST['select-orientacao-sexual']));
		$objPerfil->setProfissao(trim($_POST['txt-profissao']));
		$objPerfil->setReligiao(trim($_POST['txt-religiao']));
		$objPerfil->setStatus_id(trim($_POST['select-status']));
		$objPerfil->setUsuario_idUsuario($idUsuario);
		$objPerfil->setVeiculoUsuario(trim($_POST['select-veiculo']));
		
		$objPerfilDAO = new CperfilUsuarioDAO();
		if($objPerfilDAO->insert($objPerfil) > 0) {
			$cadastrou++;
		}
		else {
			echo "Falha ao tentar inserir o Perfil";
		}			
	}
	else {
		
		echo "Falha no Cadastro";
	}
	
	if($cadastrou == 2) {
		header("Location: perfil.html");
	}
?>