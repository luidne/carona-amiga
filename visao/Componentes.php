<?php

class Componentes {
	
	public function getSelectCurso() {
		
		require_once ('../daos/CcursoDAO.php');
		require_once ('../daos/CturnoDAO.php');
		
		$objCursoDAO = new CcursoDAO();
		$objTurnoDAO = new CturnoDAO();
		
		$htmlSelectCurso = "<select name='select-curso' id='select-curso' class='validate[required] field' >";
		$htmlSelectCurso .= "<option value=''>Selecione uma opção...</option>";
		
		$arrayCursos = $objCursoDAO->selectAll();
		
		for($i=0; $i<count($arrayCursos); $i++) {
			
			$rowTurnoDAO = $objTurnoDAO->selectPorId($arrayCursos[$i]->getTurno_id());
			
			$htmlSelectCurso .= "<option value='". $arrayCursos[$i]->getId() ."'>". 
									$arrayCursos[$i]->getCurso() ." - ". $rowTurnoDAO['turno'] .
								"</option>";
		}
		
		$htmlSelectCurso .= "</select>";
		
		echo $htmlSelectCurso;
	}
	
	
	public function getSelectStatus() {
		
		require_once ('../daos/CstatusDAO.php');	
		
		$objStatusDAO = new CstatusDAO();
		
		$htmlSelectStatus = "<select name='select-status' id='select-status' class='validate[required] field' >";
		$htmlSelectStatus .= "<option value=''>Selecione uma opção...</option>";
		
		$arrayStatus = $objStatusDAO->selectAll();
		
		for($i=0; $i<count($arrayStatus); $i++) {			
			
			$htmlSelectStatus .= "<option value='". $arrayStatus[$i]->getId() ."'>". 
									$arrayStatus[$i]->getStatus().
								"</option>";
		}
		
		$htmlSelectStatus .= "</select>";
		
		echo $htmlSelectStatus;
	}
}
?>