<!DOCTYPE html>
<html>
    <head>
        <title>Bem-vindo ao Carona Amiga</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../resources/metro/css/modern.css">
        <link rel="stylesheet" href="../resources/metro/css/modern-responsive.css">
        <link rel="stylesheet" href="../resources/meu_estilo.css">

        <script src="../resources/metro/js/jquery-1.8.2.min.js"></script>
        <script src="../resources/metro/js/carousel.js" ></script>        
    </head>
    <body>
        <div class="back_topo" style="width: 100%;">

        </div>
        <div style="position: absolute;
             top: 50%;
             left: 50%;
             margin: -200px auto auto -500px;
             width: 1000px;
             height: 400px" >
            <div style="position: absolute;
                 right: -5px;
                 top: -60.5px;
                 z-index: 9999999;">
                <h1>Carona Amiga</h1>
            </div>
            <div class="span12" style="height: 400px; width: 100%;">
                <div class="page " style="background: #ddd;width: 70%;">
                    <div class="span5" style="width: 100%; height: 100%;">
                        <div class="carousel span5" style="width: 100%; height: 100%;" data-role="carousel" data-param-effect="slowdown" data-param-direction="left" data-param-period="3000" data-param-markers="off">
                            <div class="slides">
                                <div class="slide image" id="slide1">
                                    <img src="../resources/metro/images/1.jpg">
                                    <div class="description">Mensagem do dia.</div>
                                </div>
                                <div class="slide image" id="slide2">
                                    <img src="../resources/metro/images/2.jpg">
                                </div>
                                <div class="slide image" id="slide3">
                                    <img src="../resources/metro/images/3.jpg">
                                </div>
                            </div>

                            <span class="control left bg-color-green">‹</span>
                            <span class="control right bg-color-green">›</span>

                        </div>
                    </div>
                </div>
                <form action="principal.php">
                    <div class="charms" style=";width: 30%;">
                        <div style="background: #00bb00; position: relative; top: -10px;">
                            <center>
                                <h2 style="color: white;">Autenticação</h2>
                            </center>
                            <table style="border: none;">
                                <tr>
                                    <td class="right" style="vertical-align: top; border: none;" >
                                        <h3 style="color: white; line-height: 10px; position: relative; right: -15px;">
                                            Usuário: 
                                        </h3>
                                    </td>
                                    <td style="border: none;">
                                        <div class="input-control text disabled" style="200px;">
                                            <input type="text" value="">
                                        </div>            
                                    </td>
                                </tr>
                                <tr>
                                    <td class="right" style="vertical-align: top; border: none;" >
                                        <h3 style="color: white; line-height: 10px; position: relative; right: -15px;">
                                            Senha: 
                                        </h3>
                                    </td>
                                    <td style="border: none;">
                                        <div class="input-control password disabled" style="200px;">
                                            <input type="password" value="">
                                        </div>            
                                    </td>
                                </tr>
                                <tr>
                                    <td class="right" style="vertical-align: top; border: none;" >

                                    </td>
                                    <td style="border: none;">
                                        <div class="input-control password disabled" style="200px;">
                                            <input type="submit" value="Entrar">
                                        </div>            
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="background: #00bb00; position: relative; top: -25px;">
                            <center>
                                <h3 style="color: white;">Ainda não sou cadastrado</h3>
                                <a class="button" style="background: #008287; color: white;" href="cadastro.php">Cadastrar agora</a>
                            </center>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="back_rodape" style="width: 100%; 
             position: fixed; 
             bottom: 0px;">

        </div>
    </body>
</html>
