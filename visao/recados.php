<!DOCTYPE html>
<html>
    <head>
        <title>Carona Amiga - Recados</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../resources/metro/css/modern.css">
        <link rel="stylesheet" href="../resources/metro/css/modern-responsive.css">
        <link rel="stylesheet" href="../resources/meu_estilo.css">

        <script src="../resources/metro/js/jquery-1.8.2.min.js"></script>
        <script src="../resources/metro/js/carousel.js" ></script>        
        <script src="../resources/js/fixaPainelPerfil.js" ></script>        
    </head>
    <body>
        <div class="page secondary back_topo">            
            <div class="page-region">
                <div class=" page page-region-content" style="width: 1250px;">
                    <div class="grid" style="margin-bottom: 0;">
                        <!--[Início] Painel do Cabeçalho -->
                        <?php include 'template/cabecalho.php'; ?>
                        <!--[Fim] Painel do Cabeçalho -->
                    </div>
                    <div class="grid ">
                        <div class="row" style="height: 16px;">
                            <img src="../resources/img/dobra_esquerda.png" />
                        </div>
                        <div class="row">
                            <!--[Início] Painel de Perfil -->
                            <?php include 'template/painel_perfil.php'; ?>    
                            <!--[Fim] Painel de Perfil -->
                            <div class="span11 painel_fixo">
                                <!--[Início] Escrever recado , não será exibido se for o proprietário do perfil-->
                                    <div class="span11"> 
                                        <h3 class="fg-color-gray" style="position: relative; margin-top: -5px; float: right;">Escrever recado</h3>
                                    </div>
                                    <div class="span11">
                                        <div class="input-control textarea">
                                            <textarea ></textarea>
                                        </div>
                                        <input class="button" style="float: right;" type="submit" value="Enviar">
                                    </div>
                                    <!--[Fim] Escrever recado -->
                                <div class="span11"> 
                                    <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Recados</h3>
                                </div>
                                <div class="span11">
                                    <!--[Início] Painel de Recados -->                                    
                                    <div class="span11" style="height: 20px;"></div>
                                    <div class=" span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <img src="../resources/img/dobra_direita.png" style="position: relative;
                                             right: -20px;
                                             top: -16px;
                                             float: right;" />
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <!--[Fim] Painel do Feed de Atividades -->
                                </div>             
                            </div>
                        </div>
                        <!--[Início] Painel do Rodapé -->
                        <div class="row bg-color-gray" style="text-align: right; width: 100%;">
                            <br/>
                            <br/>
                            <div class="span3">

                            </div>
                            <div class="span11" style="border-top: 1px #ccc dotted;">
                                <div class="horizontal-menu" style="float: center;">
                                    <ul>
                                        <li>
                                            <a class="fg-color-blue" style="" href="http://www.catolica-to.edu.br">
                                                Faculdade Católica do Tocantins
                                            </a>
                                        </li>
                                        <li> 
                                            <a style="color: #cc; font-size: 10pt;">                                  
                                                Curso de Sistemas de Informação
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--[Fim] Painel do Rodapé -->
                    </div>                    
                </div>
            </div>
        </div>
    </body>
</html>
