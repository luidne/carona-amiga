<!DOCTYPE html>
<html>
    <head>
        <title>Carona Amiga - Alterar meu Perfil</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../resources/metro/css/modern.css">
        <link rel="stylesheet" href="../resources/metro/css/modern-responsive.css">
        <link rel="stylesheet" href="../resources/meu_estilo.css">

        <script src="../resources/metro/js/jquery-1.8.2.min.js"></script>
        <script src="../resources/metro/js/carousel.js" ></script>        
        <script src="../resources/js/fixaPainelPerfil.js" ></script>        
    </head>
    <body>
        <div class="page secondary back_topo">            
            <div class="page-region">
                <div class=" page page-region-content" style="width: 1250px;">
                    <div class="grid" style="margin-bottom: 0;">
                        <!--[Início] Painel do Cabeçalho -->
                        <?php include 'template/cabecalho.php';?>
                        <!--[Fim] Painel do Cabeçalho -->
                    </div>
                    <div class="grid ">
                        <div class="row" style="height: 16px;">
                            <img src="../resources/img/dobra_esquerda.png" />
                        </div>
                        <div class="row">
                            <!--[Início] Painel de Perfil -->
                            <?php include 'template/painel_perfil.php';?>    
                            <!--[Fim] Painel de Perfil -->
                            <div class="span11 painel_fixo">
                                <!--[Fim] Alterar Dados da Conta -->
                                <div class="span11"> 
                                    <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Dados da Conta</h3>
                                </div>
                                <div class="span11 ">
                                    <table style="width: auto;" >
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Nome:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Matrícula:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>CPF:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Sexo</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>E-mail:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Senha:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control password">
                                                    <input type="password" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Telefone:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Data de Nascimento:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Curso:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Período do Curso:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Endereço:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Nome:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Ponto de Saída:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control textarea">
                                                    <textarea ></textarea>
                                                </div> 
                                            </td>
                                        </tr>                                    
                                        <tr>
                                            <td class="border-color-white" >
                                            </td>
                                            <td class="border-color-white" >
                                                <a class="button bg-color-green">Salvar alterações</a>
                                            </td>
                                        </tr>                                       
                                    </table>

                                    <hr />

                                </div>
                                <!--[Fim] Alterar Dados da Conta -->

                                <!--[Início] Alterar Meu Perfil -->
                                <div class="span11"> 
                                    <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Meu Perfil</h3>
                                </div>

                                <div class="span11">
                                    <table style="width: auto;" >
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Profissão:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Local de Trabalho:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Veículo:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Orientação Sexual:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Religião:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Fumante:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Esportes:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Músicas:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div> 
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Status:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <div class="input-control text">
                                                    <input type="text" />
                                                </div> 
                                            </td>
                                        </tr>                                    
                                        <tr>
                                            <td class="border-color-white" >
                                            </td>
                                            <td class="border-color-white" >
                                                <a class="button bg-color-green">Salvar alterações</a>
                                            </td>
                                        </tr>                                       
                                    </table>

                                    <hr />

                                </div>
                                <!--[Fim] Alterar Meu Perfil -->
                            </div>
                        </div>
                        <!--[Início] Painel do Rodapé -->
                        <div class="row bg-color-gray" style="text-align: right; width: 100%;">
                            <br/>
                            <br/>
                            <div class="span3">

                            </div>
                            <div class="span11" style="border-top: 1px #ccc dotted;">
                                <div class="horizontal-menu" style="float: center;">
                                    <ul>
                                        <li>
                                            <a class="fg-color-blue" style="" href="http://www.catolica-to.edu.br">
                                                Faculdade Católica do Tocantins
                                            </a>
                                        </li>
                                        <li> 
                                            <a style="color: #cc; font-size: 10pt;">                                  
                                                Curso de Sistemas de Informação
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--[Fim] Painel do Rodapé -->
                    </div>                    
                </div>
            </div>
        </div>
    </body>
</html>
