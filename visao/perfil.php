<!DOCTYPE html>
<html>
    <head>
        <title>Carona Amiga - Meu Perfil</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../resources/metro/css/modern.css">
        <link rel="stylesheet" href="../resources/metro/css/modern-responsive.css">
        <link rel="stylesheet" href="../resources/meu_estilo.css">

        <script src="../resources/metro/js/jquery-1.8.2.min.js"></script>
        <script src="../resources/metro/js/carousel.js" ></script>        
        <script src="../resources/js/fixaPainelPerfil.js" ></script>        
    </head>
    <body>
        <div class="page secondary back_topo">            
            <div class="page-region">
                <div class=" page page-region-content" style="width: 1250px;">
                    <div class="grid" style="margin-bottom: 0;">
                        <!--[Início] Painel do Cabeçalho -->
                        <?php include 'template/cabecalho.php'; ?>
                        <!--[Fim] Painel do Cabeçalho -->
                    </div>
                    <div class="grid ">
                        <div class="row" style="height: 16px;">
                            <img src="../resources/img/dobra_esquerda.png" />
                        </div>
                        <div class="row">
                            <!--[Início] Painel de Perfil -->
                            <?php include 'template/painel_perfil.php'; ?>    
                            <!--[Fim] Painel de Perfil -->
                            <div class="span11 painel_fixo">
                                <div class="span11"> 
                                    <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Meu Perfil</h3>
                                </div>
                                <!--[Início] Painel do Perfil Completo -->                                
                                <div class="span11">
                                    <table style="width: auto;" >
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Meu Nome:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <h4>Meu Nome</h4>                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Curso:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <h4>Meu Nome</h4>                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>E-mail:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <h4>Meu Nome</h4>                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Telefone:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <h4>Meu Nome</h4>                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Local de Trabalho:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <h4>Meu Nome</h4>                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" style="text-align: right;">
                                                <h5>Descrição:</h5>                                           
                                            </td>
                                            <td class="border-color-white" >
                                                <h4>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados.
                                                    Seções 1.10.32 e 1.10.33 de ".</h4>                                           
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-color-white" >

                                            </td>
                                            <td class="border-color-white" >
                                                <a href="alterar_perfil.php" class="button bg-color-orange">Alterar dados</a>
                                            </td>
                                        </tr>                                       
                                    </table>

                                    

                                </div>
                                
                                <!--[Fim] Painel do Perfil Completo -->
                                <div class="span11">                                
                                    <!--[Início] Painel de Recados -->
                                    <br/>                                    
                                    <div class="span11"> 
                                        <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Recados</h3>
                                    </div>
                                    <div class=" span11 bg-color-purple fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <h3 class="fg-color-white"><strong>Pedido de Carona</strong></h3>
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class="span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <h3 class="fg-color-white"><strong>Recado</strong></h3>
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-purple fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <h3 class="fg-color-white"><strong>Pedido de Carona</strong></h3>
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <!--[Fim] Painel de Recados -->

                                    <!--[Início] Painel de Pedidos de Carona -->                        
                                    <div class="span11"> 
                                        <h3 class="fg-color-gray" style="position: relative; margin-top: -5px;">Pedidos de Carona</h3>
                                    </div>

                                    <div class=" span11 bg-color-purple fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <h3 class="fg-color-white"><strong>Pedido de Carona</strong></h3>
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class="span11 bg-color-blue fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <h3 class="fg-color-white"><strong>Recado</strong></h3>
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <div class=" span11 bg-color-purple fg-color-white" style="padding-left: 20px;padding-right: 20px;">
                                        <h3 class="fg-color-white"><strong>Pedido de Carona</strong></h3>
                                        <p><strong>João: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt nulla ut nulla vehicula ut fermentum velit laoreet. Mauris sit amet augue nisl, ut iaculis ipsum. Quisque luctus pharetra ornare.</p>
                                        <p class="tertiary-secondary-text">
                                            12 de Novembro de 2012 às 10h00 
                                        </p>
                                    </div>
                                    <!--[Fim] Painel de Pedidos de Carona -->
                                </div>
                            </div>
                        </div>
                        <!--[Início] Painel do Rodapé -->
                        <div class="row bg-color-gray" style="text-align: right; width: 100%;">
                            <br/>
                            <br/>
                            <div class="span3">

                            </div>
                            <div class="span11" style="border-top: 1px #ccc dotted;">
                                <div class="horizontal-menu" style="float: center;">
                                    <ul>
                                        <li>
                                            <a class="fg-color-blue" style="" href="http://www.catolica-to.edu.br">
                                                Faculdade Católica do Tocantins
                                            </a>
                                        </li>
                                        <li> 
                                            <a style="color: #cc; font-size: 10pt;">                                  
                                                Curso de Sistemas de Informação
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--[Fim] Painel do Rodapé -->
                    </div>                    
                </div>
            </div>
        </div>
    </body>
</html>
