<?php

class Crecados{
    
    private $id = NULL;
    private $mensagem = NULL;
    private $datahora = NULL;
    private $idUsuario = NULL;
    private $idUsuario1 = NULL;
    
    function __construct($id = NULL, $mensagem = NULL, $datahora = NULL, $idUsuario = NULL, $idUsuario1 = NULL) {
        $this->id = $id;
        $this->mensagem = $mensagem;
        $this->datahora = $datahora;
        $this->idUsuario = $idUsuario;
        $this->idUsuario1 = $idUsuario1;
    }
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getMensagem() {
        return $this->mensagem;
    }

    public function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    public function getDatahora() {
        return $this->datahora;
    }

    public function setDatahora($datahora) {
        $this->datahora = $datahora;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }

    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    public function getIdUsuario1() {
        return $this->idUsuario1;
    }

    public function setIdUsuario1($idUsuario1) {
        $this->idUsuario1 = $idUsuario1;
    }



    
    
    
    
    
    
}
?>
