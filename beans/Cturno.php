<?php

class Cturno{
	
    private $id = NULL;
    private $turno = NULL;    
    
    function __construct($id = NULL, $turno = NULL) {
		
        $this->id = $id;
        $this->turno = $turno;                
    }
	
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTurno() {
        return $this->turno;
    }

    public function setTurno($turno) {
        $this->turno = $turno;
    }
}
?>
