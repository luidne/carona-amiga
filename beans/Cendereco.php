<?php

class Cendereco {

	private $id = NULL;
	private $endereco = NULL;
	private $complemento = NULL;
	private $numero = NULL;
	private $bairro = NULL;
	private $cep = NULL;
	private $cidade_id = NULL;

	

	public function __constructor ($id = NULL, $endereco = NULL, $complemento = NULL, $numero = NULL,
			$bairro = NULL,  $cep = NULL, $cidade_id = NULL) {
		
		$this->id = $id;
		$this->endereco = $endereco;
		$this->complemento = $complemento;
		$this->numero = $numero;
		$this->bairro = $bairro;
		$this->cep = $cep;
		$this->cidade_id = $cidade_id;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getEndereco() {
		return $this->endereco;
	}

	public function setEndereco($endereco) {
		$this->endereco = $endereco;
	}

	public function getComplemento() {
		return $this->complemento;
	}

	public function setComplemento($complemento) {
		$this->complemento = $complemento;
	}

	public function getNumero() {
		return $this->numero;
	}

	public function setNumero($numero) {
		$this->numero = $numero;
	}

	public function getBairro() {
		return $this->bairro;
	}

	public function setBairro($bairro) {
		$this->bairro = $bairro;
	}

	public function getCep() {
		return $this->cep;
	}

	public function setCep($cep) {
		$this->cep = cep;
	}

	public function getCidade_id() {
		return $this->cidade_id;
	}

	public function setCidade_id($cidade_id) {
		$this->cidade_id = $cidade_id;
	}

}

?>
