<?php

class CtipoUsuario {
    
    private $id = NULL;
    private $tipo = NULL;
    
    function __construct($id = NULL, $tipo = NULL) {
        $this->id = $id;
        $this->tipo = $tipo;
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }



    
    
}
?>
