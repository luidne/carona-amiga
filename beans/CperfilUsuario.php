<?php

class CperfilUsuario {

   private $Usuario_idUsuario = NULL;
   private $profissao = NULL;
   private $localDeTrabalho = NULL;
   private $veiculoUsuario = NULL;
   private $orientacaoSexual = NULL;
   private $religiao = NULL;
   private $fumante = NULL;
   private $esportes = NULL;
   private $musicas = NULL;
   private $descricao = NULL;
   private $Status_id = NULL;
   
   function __construct($Usuario_idUsuario = NULL, $profissao = NULL, $localDeTrabalho = NULL, 
           $veiculoUsuario = NULL, $orientacaoSexual = NULL, $religiao = NULL, $fumante = NULL, $esportes = NULL,
           $musicas = NULL, $descricao = NULL, $Status_id = NULL) {
       $this->Usuario_idUsuario = $Usuario_idUsuario;
       $this->profissao = $profissao;
       $this->localDeTrabalho = $localDeTrabalho;
       $this->veiculoUsuario = $veiculoUsuario;
       $this->orientacaoSexual = $orientacaoSexual;
       $this->religiao = $religiao;
       $this->fumante = $fumante;
       $this->esportes = $esportes;
       $this->musicas = $musicas;
       $this->descricao = $descricao;
       $this->Status_id = $Status_id;
   }
   
   public function getUsuario_idUsuario() {
       return $this->Usuario_idUsuario;
   }

   public function setUsuario_idUsuario($Usuario_idUsuario) {
       $this->Usuario_idUsuario = $Usuario_idUsuario;
   }

   public function getProfissao() {
       return $this->profissao;
   }

   public function setProfissao($profissao) {
       $this->profissao = $profissao;
   }

   public function getLocalDeTrabalho() {
       return $this->localDeTrabalho;
   }

   public function setLocalDeTrabalho($localDeTrabalho) {
       $this->localDeTrabalho = $localDeTrabalho;
   }

   public function getVeiculoUsuario() {
       return $this->veiculoUsuario;
   }

   public function setVeiculoUsuario($veiculoUsuario) {
       $this->veiculoUsuario = $veiculoUsuario;
   }

   public function getOrientacaoSexual() {
       return $this->orientacaoSexual;
   }

   public function setOrientacaoSexual($orientacaoSexual) {
       $this->orientacaoSexual = $orientacaoSexual;
   }

   public function getReligiao() {
       return $this->religiao;
   }

   public function setReligiao($religiao) {
       $this->religiao = $religiao;
   }

   public function getFumante() {
       return $this->fumante;
   }

   public function setFumante($fumante) {
       $this->fumante = $fumante;
   }

   public function getEsportes() {
       return $this->esportes;
   }

   public function setEsportes($esportes) {
       $this->esportes = $esportes;
   }

   public function getMusicas() {
       return $this->musicas;
   }

   public function setMusicas($musicas) {
       $this->musicas = $musicas;
   }

   public function getDescricao() {
       return $this->descricao;
   }

   public function setDescricao($descricao) {
       $this->descricao = $descricao;
   }

   public function getStatus_id() {
       return $this->Status_id;
   }

   public function setStatus_id($Status_id) {
       $this->Status_id = $Status_id;
   }



   

   
   
}
?>
