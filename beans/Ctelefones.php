<?php

class Ctelefones{

    private $id = NULL;
	private $codigoDeArea = NULL;
	private $telefone = NULL;
	private $tipoTelefone_id = NULL;
	private $Usuario_idUsuario = NULL;
	
	
	function __construct($id = NULL, $codigoDeArea = NULL, $telefone = NULL, $tipoTelefone_id = 3, $Usuario_idUsuario = NULL) {
		$this->id = $id;
		$this->codigoDeArea = $codigoDeArea;
		$this->telefone = $telefone;
		$this->tipoTelefone_id = $tipoTelefone_id;
		$this->Usuario_idUsuario = $Usuario_idUsuario;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getCodigoDeArea() {
		return $this->codigoDeArea;
	}

	public function setCodigoDeArea($codigoDeArea) {
		$this->codigoDeArea = $codigoDeArea;
	}

	public function getTelefone() {
		return $this->telefone;
	}

	public function setTelefone($telefone) {
		$this->telefone = $telefone;
	}

	public function getTipoTelefone_id() {
		return $this->tipoTelefone_id;
	}

	public function setTipoTelefone_id($tipoTelefone_id) {
		$this->tipoTelefone_id = $tipoTelefone_id;
	}
	
	public function getUsuario_idUsuario() {
		return $this->Usuario_idUsuario;
	}

	public function setUsuario_idUsuario($Usuario_idUsuario) {
		$this->Usuario_idUsuario = $Usuario_idUsuario;
	}
}
?>
