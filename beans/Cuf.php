<?php

class Cuf{
    
        private $id = NULL;
	private $uf = NULL;
	private $nome = NULL;
        
        function __construct($id = NULL, $uf = NULL, $nome = NULL) {
            $this->id = $id;
            $this->uf = $uf;
            $this->nome = $nome;
            
            
        }

        
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getUf() {
            return $this->uf;
        }

        public function setUf($uf) {
            $this->uf = $uf;
        }

        public function getNome() {
            return $this->nome;
        }

        public function setNome($nome) {
            $this->nome = $nome;
        }


    
}
?>
