<?php

class Ccurso{
    private $id = NULL;
    private $curso = NULL;
    private $Campus_id = NULL;
	private $Turno_id = NULL;
    
    function __construct($id = NULL, $curso = NULL, $Campus_id = NULL, $Turno_id = NULL) {
		
        $this->id = $id;
        $this->curso = $curso;        
        $this->Campus_id = $Campus_id;
		$this->Turno_id = $Turno_id;
    }
	
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCurso() {
        return $this->curso;
    }

    public function setCurso($curso) {
        $this->curso = $curso;
    }

    public function getTurno_id() {
        return $this->Turno_id;
    }

    public function setTurno_id($Turno_id) {
        $this->Turno_id = $Turno_id;
    }

    public function getCampus_id() {
        return $this->Campus_id;
    }

    public function setCampus_id($Campus_id) {
        $this->Campus_id = $Campus_id;
    }


    
}
?>
