<?php

 class Cusuario{
     
     private $idUsuario = NULL;
     private $nome = NULL;
     private $matricula = NULL;
     private $cpf = NULL;
     private $sexo = NULL;
     private $email = NULL;
     private $dataNascimento = NULL;
     private $periodoCurso = NULL;
     private $senha = NULL;
     private $TipoUsuario_id = NULL;
     private $Cursos_id = NULL;
     private $Endereco_id = NULL;
     private $PontoDeSaida_id = NULL;
     
     function __construct($idUsuario = NULL, $nome = NULL, $matricula = NULL, $cpf = NULL, 
             $sexo = NULL, $email = NULL, $dataNascimento = NULL, $periodoCurso = NULL, 
             $senha = NULL, $TipoUsuario_id = NULL, $Cursos_id = NULL, $Endereco_id = NULL, $PontoDeSaida_id = NULL) {
         $this->idUsuario = $idUsuario;
         $this->nome = $nome;
         $this->matricula = $matricula;
         $this->cpf = $cpf;
         $this->sexo = $sexo;
         $this->email = $email;
         $this->dataNascimento = $dataNascimento;
         $this->periodoCurso = $periodoCurso;
         $this->senha = $senha;
         $this->TipoUsuario_id = $TipoUsuario_id;
         $this->Cursos_id = $Cursos_id;
         $this->Endereco_id = $Endereco_id;
         $this->PontoDeSaida_id = $PontoDeSaida_id;
     }

     public function getIdUsuario() {
         return $this->idUsuario;
     }

     public function setIdUsuario($idUsuario) {
         $this->idUsuario = $idUsuario;
     }

     public function getNome() {
         return $this->nome;
     }

     public function setNome($nome) {
         $this->nome = $nome;
     }

     public function getMatricula() {
         return $this->matricula;
     }

     public function setMatricula($matricula) {
         $this->matricula = $matricula;
     }

     public function getCpf() {
         return $this->cpf;
     }

     public function setCpf($cpf) {
         $this->cpf = $cpf;
     }

     public function getSexo() {
         return $this->sexo;
     }

     public function setSexo($sexo) {
         $this->sexo = $sexo;
     }

     public function getEmail() {
         return $this->email;
     }

     public function setEmail($email) {
         $this->email = $email;
     }

     public function getDataNascimento() {
         return $this->dataNascimento;
     }

     public function setDataNascimento($dataNascimento) {
         $this->dataNascimento = $dataNascimento;
     }

     public function getPeriodoCurso() {
         return $this->periodoCurso;
     }

     public function setPeriodoCurso($periodoCurso) {
         $this->periodoCurso = $periodoCurso;
     }

     public function getSenha() {
         return $this->senha;
     }

     public function setSenha($senha) {
         $this->senha = $senha;
     }

     public function getTipoUsuario_id() {
         return $this->TipoUsuario_id;
     }

     public function setTipoUsuario_id($TipoUsuario_id) {
         $this->TipoUsuario_id = $TipoUsuario_id;
     }

     public function getCursos_id() {
         return $this->Cursos_id;
     }

     public function setCursos_id($Cursos_id) {
         $this->Cursos_id = $Cursos_id;
     }

     public function getEndereco_id() {
         return $this->Endereco_id;
     }

     public function setEndereco_id($Endereco_id) {
         $this->Endereco_id = $Endereco_id;
     }

     public function getPontoDeSaida_id() {
         return $this->PontoDeSaida_id;
     }

     public function setPontoDeSaida_id($PontoDeSaida_id) {
         $this->PontoDeSaida_id = $PontoDeSaida_id;
     }


     
     
     
 }
?>
