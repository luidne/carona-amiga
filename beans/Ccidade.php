<?php
 class cCidade {

	private $id = NULL;
	private $nome = NULL;
	private $UF_id = NULL;
	

	public function __construct($id = NULL, $nome = NULL, $uF_id = NULL) {		
		$this->id = $id;
		$this->nome = $nome;
		$this->UF_id = $uF_id;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getNome() {
		return $this->nome;
	}

	public function setNome($nome) {
		$this->nome = $nome;
	}

	public function getUF_id() {
		return $this->UF_id;
	}

	public function setUF_id($uF_id) {
                $this->UF_id = $uF_id;
	}
}

?>
