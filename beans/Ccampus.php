<?php

class Ccampus{
    
    private $id = NULL;
    private $campus = NULL;
    
    function __construct($id = NULL, $campus = NULL) {
        $this->id = $id;
        $this->campus = $campus;
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCampus() {
        return $this->campus;
    }

    public function setCampus($campus) {
        $this->campus = $campus;
    }



    
    
}
?>
