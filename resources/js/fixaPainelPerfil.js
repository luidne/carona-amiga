/*
 *@author: Luídne Mota
 **/

var x = 0; //coordernada X da posiÃ§Ã£o da barra de rolagem(scroll)

//Descrição: Função que chama funÃ§Ã£o Show ou Hide de acordo com a coordernada X da barra de rolagem.
jQuery(document).ready(function() {
    height = $(window).height();
    $("#painel_perfil").css("height", height);           
    jQuery(window).scroll(function() {
        x = jQuery(window).scrollTop();
        if (x >= 160) {
            $("#painel_perfil").css("position", "fixed");
            $("#painel_perfil").css("top", "0");
            $("#painel_perfil").css("height", height);           
        }
        if (x <= 159) {
            $("#painel_perfil").css("position", "relative");
            $("#painel_perfil").css("top", "auto");
        }
    }); 
});
