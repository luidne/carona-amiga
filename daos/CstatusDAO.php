<?PHP
require_once ('../bd/ConnectionMysql.php');
require_once ('../beans/Cstatus.php');

class CstatusDAO
{
    private $connection = NULL;


    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function insert(Cstatus $obj)
    {
        $sql = "INSERT INTO Status (status) 
                VALUES ('". $obj->getStatus() ."')";
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorId($id)
    {
        $sql = "UPDATE Status SET status='N'WHERE id=". $id;
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorObj(Cstatus $obj)
    {
        $sql = "UPDATE Status SET status='N' WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function update(Categoria $obj)
    {
        $sql = "UPDATE Status SET status='". $obj->getStatus() ."'WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function selectAll()
    {
        $sql = "SELECT id, status FROM status";
		
		$result = $this->connection->queryConnect($sql); 
     
        $array = array();
		
		while($row = mysql_fetch_array($result)) {
			
			$obj = new Cstatus();
			$obj->setId($row['id']);
			$obj->setStatus($row['status']);
						
			array_push($array, $obj);
		}
		
		return $array;           
    }
    
    
    public function selectPoId($id)
    {
        $sql = "SELECT id, status FROM Status
                WHERE id=". $id;

        $result = $this->connection->queryConnect($sql);
        
        if (mysql_num_rows($result) > 0)
        {
            $obj = new Cstatus();
            $obj->setId($id);
            $obj->setStatys(mysql_result($result, 0, "status"));
            

            return $obj;
        }
        else
        {
            return NULL;
        }
    }
}
?>