<?php
require_once ('../bd/ConnectionMysql.php');
require_once ('../beans/Ccurso.php');

class CcursoDAO
{
    private $connection = NULL;


    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }
	
	
	public function selectAll() {
		
		$sql = "SELECT id, cursos, Campus_id, Turno_id FROM cursos c;";
     
        $result = $this->connection->queryConnect($sql); 
		
		$array = array();
		
		while($row = mysql_fetch_array($result)) {
			
			$obj = new Ccurso();
			$obj->setId($row['id']);
			$obj->setCurso($row['cursos']);			
			$obj->setCampus_id($row['Campus_id']);
			$obj->setTurno_id($row['Turno_id']);
			
			array_push($array, $obj);
		}
		
		return $array;
	}	
}

?>