<?PHP
require_once ('../bd/ConnectionMysql.php');
require_once ('../beans/Cusuario.php');

class CusuarioDAO
{
    private $connection = NULL;


    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function insert(Cusuario $obj)
    {
        $sql = "INSERT INTO usuario
				  (nome, matricula, cpf, sexo, email, dataNascimento, periodoCurso, senha, TipoUsuario_id, Cursos_id, Endereco_id, 
				   PontoDeSaida_id)
				VALUES
				  ('". $obj->getNome() ."', '". $obj->getMatricula() ."', ". $obj->getCpf() .", '". $obj->getSexo() ."', '". 
				   $obj->getEmail() ."', '". $obj->getDataNascimento() ."', ". $obj->getPeriodoCurso() .", '". $obj->getSenha() ."', 2, ". 
				   $obj->getCursos_id() .", ". $obj->getEndereco_id() .", ". $obj->getPontoDeSaida_id() .")";
				
        if($this->connection->queryConnect($sql)) {
			
			return $this->selectIdUltimoRegistro();
		}
		else {
			
			return -1;
		}
    }
	
		    
    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function update(Categoria $obj)
    {
        $sql = "UPDATE Campus SET campus='". $obj->getCampus() ."'WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function select()
    {
        $sql = "SELECT * FROM campus ";
     
        return $this->connection->queryConnect($sql);       
           
    }
    
    
    public function selectPoId($id)
    {
        $sql = "SELECT id, campus FROM Campus 
                WHERE id=". $id;

        $result = $this->connection->queryConnect($sql);
        
        if (mysql_num_rows($result) > 0)
        {
            $obj = new Ccampus();
            $obj->setId($id);
            $obj->setCampus(mysql_result($result, 0, "campus"));
            

            return $obj;
        }
        else
        {
            return NULL;
        }
    }
	
	
	public function selectIdUltimoRegistro(){
		
		$sql = "SELECT idUsuario FROM usuario ORDER BY idUsuario DESC LIMIT 1";
		
		$result = $this->connection->queryConnect($sql);
		
		$row = mysql_fetch_array($result);
		
		return $row['idUsuario'];
	}
}
?>