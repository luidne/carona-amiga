<?PHP
require ('../bd/ConnectionMysql.php');
require ('../beans/Ccampus.php');

class CcampusDAO
{
    private $connection = NULL;


    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function insert(Ccampus $obj)
    {
        $sql = "INSERT INTO Campus (campus) 
                VALUES ('". $obj->getCampus() ."')";
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorId($id)
    {
        $sql = "UPDATE Campus SET campus='N'WHERE id=". $id;
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorObj(Ccampus $obj)
    {
        $sql = "UPDATE Campus SET campus='N' WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function update(Categoria $obj)
    {
        $sql = "UPDATE Campus SET campus='". $obj->getCampus() ."'WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function select()
    {
        $sql = "SELECT * FROM campus ";
     
        return $this->connection->queryConnect($sql);       
           
    }
    
    
    public function selectPoId($id)
    {
        $sql = "SELECT id, campus FROM Campus 
                WHERE id=". $id;

        $result = $this->connection->queryConnect($sql);
        
        if (mysql_num_rows($result) > 0)
        {
            $obj = new Ccampus();
            $obj->setId($id);
            $obj->setCampus(mysql_result($result, 0, "campus"));
            

            return $obj;
        }
        else
        {
            return NULL;
        }
    }
}
?>