<?PHP
require ('../bd/ConnectionMysql.php');
require ('../beans/Cuf.php');

class CategoriaDAO
{
    private $connection = NULL;


    /*************************************************************************
    * Name: __construct
    * Description: Construtor da Classe. Inicializar os Atributos.
    * Parameters: 
    * Returns: Nenhum
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function insert(Cuf $obj)
    {
        $sql = "INSERT INTO UF (uf, nome) 
                VALUES ('". $obj->getUf() ."', '". $obj->getNome() ."')";
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorId($id)
    {
        $sql = "UPDATE UF SET uf='N'WHERE id=". $id;
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorObj(Cuf $obj)
    {
        $sql = "UPDATE UF SET isAtivo='N' WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function update(Cuf $obj)
    {
        $sql = "UPDATE UF SET uf='". $obj->getUf() ."', nome='". $obj->getNome() ."'WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function select()
    {
        $sql = "SELECT * FROM UF ";
     
        return $this->connection->queryConnect($sql);
        
       
    }    
    
    public function selectPoId($id)
    {
        $sql = "SELECT id,uf,nome FROM UF 
                WHERE id=". $id;

        $result = $this->connection->queryConnect($sql);
        
        if (mysql_num_rows($result) > 0)
        {
            $obj = new Cuf();
            $obj->setId($id);
            $obj->setUf(mysql_result($result, 0, "uf"));
            $obj->setNome(mysql_result($result, 0, "nome"));
            

            return $obj;
        }
        else
        {
            return NULL;
        }
    }
}
?>