<?PHP
require_once ('../bd/ConnectionMysql.php');
require_once ('../beans/Ctelefones.php');

class CtelefonesDAO
{
    private $connection = NULL;


    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function insert(Ctelefones $obj)
    {
        $sql = "INSERT INTO telefone
				  (codigoDeArea, telefone, tipoTelefone_id, Usuario_idUsuario)
				VALUES
				  (". $obj->getCodigoDeArea() .", ". $obj->getTelefone() .", ". $obj->getTipoTelefone_id() .", ". 
				   $obj->getUsuario_idUsuario() .")";
				
        if($this->connection->queryConnect($sql)) {
			
			return $this->selectIdUltimoRegistro();
		}
		else {
			
			return -1;
		}
    }
	
		    
    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function update(Categoria $obj)
    {
        $sql = "UPDATE Campus SET campus='". $obj->getCampus() ."'WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function select()
    {
        $sql = "SELECT * FROM campus ";
     
        return $this->connection->queryConnect($sql);       
           
    }
    
    
    public function selectPoId($id)
    {
        $sql = "SELECT id, campus FROM Campus 
                WHERE id=". $id;

        $result = $this->connection->queryConnect($sql);
        
        if (mysql_num_rows($result) > 0)
        {
            $obj = new Ccampus();
            $obj->setId($id);
            $obj->setCampus(mysql_result($result, 0, "campus"));
            

            return $obj;
        }
        else
        {
            return NULL;
        }
    }
	
	
	public function selectIdUltimoRegistro(){
		
		$sql = "SELECT id FROM telefone ORDER BY id DESC LIMIT 1";
		
		$result = $this->connection->queryConnect($sql);
		
		$row = mysql_fetch_array($result);
		
		return $row['id'];
	}
}
?>