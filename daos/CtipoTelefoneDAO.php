<?PHP
require ('../bd/ConnectionMysql.php');
require ('../beans/CtipoTelefone.php');

class CategoriaDAO
{
    private $connection = NULL;


    public function __construct()
    {
        $this->connection = new ConnectionMysql(); // Cria a Conexão.
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function insert(CtipoTelefone $obj)
    {
        $sql = "INSERT INTO TipoTelefone (tipo) 
                VALUES ('". $obj->getTipo() ."')";
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorId($id)
    {
        $sql = "UPDATE TipoTelefone SET tipo='N'WHERE id=". $id;
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function deletePorObj(CtipoTelefone $obj)
    {
        $sql = "UPDATE TipoTelefone SET tipo='N' WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function update(CtipoTelefone $obj)
    {
        $sql = "UPDATE TipoTelefone SET tipo='". $obj->getTipo() ."'WHERE id=". $obj->getId();
        return $this->connection->queryConnect($sql);
    }


    /*************************************************************************
    * Name: 
    * Description: 
    * Parameters: 
    * Returns: 
    * Author: Felipe O. Simões
    * Last Modified: 07/04/2012 - Felipe O. Simões
    *************************************************************************/
    public function select()
    {
        $sql = "SELECT * FROM TipoTelefone ";
     
        return $this->connection->queryConnect($sql);       
           
    }
    
    
    public function selectPoId($id)
    {
        $sql = "SELECT id, tipo FROM TipoTelefone 
                WHERE id=". $id;

        $result = $this->connection->queryConnect($sql);
        
        if (mysql_num_rows($result) > 0)
        {
            $obj = new CtipoTelefone();
            $obj->setId($id);
            $obj->setTipo(mysql_result($result, 0, "tipo"));
            

            return $obj;
        }
        else
        {
            return NULL;
        }
    }
}
?>